from app import db, bcrypt
from datetime import datetime
ROLE_USER = 0
ROLE_ADMIN = 1


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), unique=True)
    firstname = db.Column(db.String(20))
    lastname = db.Column(db.String(20))
    password = db.Column(db.String)
    email = db.Column(db.String(100), unique=True)
    time_registered = db.Column(db.DateTime)
    tagline = db.Column(db.String(255))
    bio = db.Column(db.Text)
    avatar = db.Column(db.String(255))
    active = db.Column(db.Boolean)

    questions = db.relationship('Question', backref='user')
    answers = db.relationship('Answer', backref='user')

    def __init__(self, username=None, password=None, email=None, firstname=None,
                 lastname=None, tagline=None, bio=None, avatar='images/noavatar.png', active=None):
        self.username = username
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.password = bcrypt.generate_password_hash(password)
        self.tagline = tagline
        self.bio = bio
        self.avatar = avatar
        self.active = active

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)  # python 3

    def __repr__(self):
        return '<User %r>' % self.username


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250))
    text = db.Column(db.Text)
    pub_date = db.Column(db.DateTime)
    tags = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    answers = db.relationship('Answer', backref='question')

    def __init__(self, title, text, user, pub_date=None, tags=None):
        self.title = title
        self.text = text
        if pub_date is None:
            pub_date = datetime.utcnow()
        self.pub_date = pub_date
        self.user_id = user
        self.tags = tags

    def __repr__(self):
        return '<Question %r>' % self.title


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    date = db.Column(db.DateTime)
    like = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))

    def __init__(self, text, user, question, date=None):
        self.text = text
        if date is None:
            date = datetime.utcnow()
        self.date = date
        self.user_id = user
        self.question_id = question

    def __repr__(self):
        return '<Answer %r>' % self.text
from flask import Flask, session, request, flash, url_for, redirect, render_template, abort, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, db, login_manager, bcrypt
from app.forms import SignupForm, SigninForm, QuestionForm, AnswerForm
from app.models import User, Question, Answer
from datetime import timedelta


@app.route('/')
@app.route('/index')
def index():
    questions = Question.query.order_by(Question.pub_date.desc()).all()
    answers_count = Answer.query.filter(Answer.id == Question.id).count()
    return render_template('index.html', questions=questions, answers_count=answers_count)


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        form = SignupForm(request.form)
        if form.validate():
            user_exist = User.query.filter_by(username=form.username.data).first()
            email_exist = User.query.filter_by(email=form.email.data).first()
            if user_exist:
                form.username.errors.append('Username already taken')
            if email_exist:
                form.email.errors.append('Email already use')
            if user_exist or email_exist:
                return render_template('register.html', form=form, title='Signup to Questionnaire')
            else:
                user = User(username=form.username.data, password=form.password.data, email=form.email.data)
                db.session.add(user)
                db.session.commit()
                flash('User successfully registered')
                login_user(user)
                return redirect(request.args.get('next') or url_for('index'))

        else:
            return render_template('register.html', form=form, title='Signup to Questionnaire')
    return render_template('register.html', form=SignupForm(), title='Signup to Questionnaire')


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        if current_user is not None and current_user.is_authenticated():
            return redirect(url_for('index'))
        form = SigninForm(request.form)
        if form.validate():
            user = User.query.filter_by(username=form.username.data).first()
            if user is None:
                form.username.errors.append('Username not found')
                return render_template('login.html',  signinpage_form=form, title='Sign In to Questionnaire')
            if not bcrypt.check_password_hash(user.password, form.password.data):
                form.password.errors.append('Password did not match')
                return render_template('login.html',  signinpage_form=form, title='Sign In to Questionnaire')
            login_user(user, remember=form.remember_me.data)
            session['signed'] = True
            session['username'] = user.username
            if session.get('next'):
                next_page = session.get('next')
                session.pop('next')
                return redirect(next_page)
            else:
                return redirect(url_for('index'))
        return render_template('login.html',  signinpage_form=form, title='Sign In to Questionnaire')
    else:
        session['next'] = request.args.get('next')
        return render_template('login.html', signinpage_form=SigninForm(), title='Sign In to Questionnaire')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.before_request
def before_request():
    g.user = current_user


@app.route('/new', methods=['GET', 'POST'])
@login_required
def new():
    if request.method == 'POST':
        form = QuestionForm()
        if form.validate():
            user = g.user.id
            question_exist = Question.query.filter_by(title=form.title.data).first()
            if question_exist:
                form.title.errors.append('Username already taken')
            else:
                question = Question(form.title.data, form.text.data, user, tags=form.tags.data)
                db.session.add(question)
                db.session.commit()
                flash('Question was successfully created')
                return redirect(url_for('index'))
    return render_template('new.html', form=QuestionForm())


@app.route('/question/<int:question_id>', methods=['GET', 'POST'])
def show_or_update(question_id):
    question_item = Question.query.get(question_id)
    form = AnswerForm()
    if request.method == 'GET':
        return render_template('view.html', question=question_item, form=form)
    if question_item.user.id == g.user.id:
        question_item.title = request.form['title']
        question_item.text = request.form['text']
        question_item.done = ('done.%d' % question_id) in request.form
        db.session.commit()
        return redirect(url_for('index'))
    flash('You are not authorized to edit this todo item', 'error')
    return redirect(url_for('show_or_update', question_id=question_id))


@app.route('/user/<username>')
@login_required
def user(username):
    usr = User.query.filter_by(username=username).first()
    if usr == None:
        flash('User ' + username + ' not found.')
        return redirect(url_for('index'))
    questions = Question.query.filter(Question.user_id == usr.id)
    return render_template('user.html', user=usr, questions=questions)


@app.route('/question/addanswer/<int:question_id>', methods=['GET', 'POST'])
@login_required
def addanswer(question_id):
    if request.method == 'POST':  # and ('pause' not in session)
        form = AnswerForm()
        if form.validate():
            answer = Answer(text=form.text.data, user=g.user.id, question=question_id)
            db.session.add(answer)
            db.session.commit()
            #session['pause'] = True
            #session.permanent = True
            #app.permanent_session_lifetime = timedelta(minutes=1)
            return redirect(url_for('show_or_update', question_id=question_id))
    return redirect(url_for('show_or_update', question_id=question_id))


@app.route('/set_cookie')
@app.route('/question/addlike/<int:answer_id>', methods=['GET', 'POST'])
@login_required
def add_like(answer_id):
    if str(answer_id) in request.cookies:
        answer = Answer.query.filter_by(id=answer_id).first()
        question_id = answer.question_id
        return redirect(url_for('show_or_update', question_id=question_id))
    else:
        answer = Answer.query.filter_by(id=answer_id).first()
        question_id = answer.question_id
        answer.like += 1
        db.session.commit()
        redirect_to_curr = redirect(url_for('show_or_update', question_id=question_id))
        response = app.make_response(redirect_to_curr)
        response.set_cookie(str(answer_id), value='like')
        return response

@app.route('/<order>')
def order(order):
    if order == 'newest':
        questions = Question.query.order_by(Question.pub_date.desc()).all()
        return render_template('index.html', questions=questions)
